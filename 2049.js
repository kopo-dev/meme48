(function($) {
  $.fn.game2049 = function() {
    var tab;
    var score = 0;

    initGame();

    function initGame() {
      initTab();
      setRandomCell();
      setRandomCell();
      printTab();
    }

    function initTab() {
      tab = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
      console.table(tab);
    }

    function printTab() {
      for (y = 0; y < 4; y++) {
        for (x = 0; x < 4; x++) {
          $(".game-container").append(
            "<div id='" +
              x +
              "-" +
              y +
              "' class='cell-" +
              tab[y][x] +
              "'>" +
              tab[y][x] +
              "</div>"
          );
        }
      }
    }

    function refreshTab() {
      for (y = 0; y < 4; y++) {
        for (x = 0; x < 4; x++) {
          $("#" + x + "-" + y).removeClass(function(index, css) {
            return css.match(/cell-[0-9]*/)
          })
          $("#" + x + "-" + y).text(tab[y][x])
          $("#" + x + "-" + y).addClass("cell-" + tab[y][x])
        }
      }
    }

    function isGameOver() {}

    function isMergePossible() {}

    function isTabFull() {
      for (y = 0; y < 4; y++) {
        for (x = 0; x < 4; x++) {
          if (tab[y][x] == 0) return false;
        }
      }
      return true;
    }

    function setRandomCell() {
      $(".new").removeClass("new")
      if (isTabFull()) {
        return false;
      }
      let x = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
      let y = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
      let rng = Math.floor(Math.random() * (3 - 0 + 1)) + 0;

      while (tab[y][x] != 0) {
        x = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
        y = Math.floor(Math.random() * (3 - 0 + 1)) + 0;
      }
      if (rng == 3) {
        tab[y][x] = 4;
        $('#' + x + "-" + y).text(4)
        $('#' + x + "-" + y).addClass("new")
        }
      else {
        tab[y][x] = 2;
        $('#' + x + "-" + y).text(4)
        $('#' + x + "-" + y).addClass("new")
        }
      }

    function handleRight() {
      let moved = false;
      let merged = false;
      moved = moveRight();
      merged = mergeRight();
      moveRight();
      return moved || merged;
    }

    function handleLeft() {
      let moved = false;
      let merged = false;
      moved = moveLeft();
      merged = mergeLeft();
      moveLeft();
      return moved || merged;
    }

    function handleUp() {
      let moved = false;
      let merged = false;
      moved = moveUp();
      merged = mergeUp();
      moveUp();
      return moved || merged;
    }

    function handleDown() {
      let moved = false;
      let merged = false;
      moved = moveDown();
      merged = mergeDown();
      moveDown();
      return moved || merged;
    }

    function moveLeft() {
      let hasMoved = false;
      for (y = 0; y < 4; y++) {
        for (x = 0; x < 4; x++) {
          if (tab[y][x] != 0) {
            let xTemp = x - 1;
            while (xTemp >= 0 && tab[y][xTemp] == 0) {
              hasMoved = true;
              tab[y][xTemp] = tab[y][xTemp + 1];
              tab[y][xTemp + 1] = 0;
              xTemp--;
            }
          }
        }
      }
      return hasMoved;
    }

    function moveRight() {
      let hasMoved = false;
      for (y = 0; y < 4; y++) {
        for (x = tab.length - 2; x >= 0; x--) {
          if (tab[y][x] != 0) {
            let xTemp = x + 1;
            while (xTemp < tab[y].length && tab[y][xTemp] == 0) {
              hasMoved = true;
              tab[y][xTemp] = tab[y][xTemp - 1];
              tab[y][xTemp - 1] = 0;
              xTemp++;
            }
          }
        }
      }
      return hasMoved;
    }

    function moveUp() {
      let hasMoved = false;
      for (x = 0; x < 4; x++) {
        for (y = 0; y < 4; y++) {
          if (tab[y][x] != 0) {
            let yTemp = y - 1;
            while (yTemp >= 0 && tab[yTemp][x] == 0) {
              hasMoved = true;
              tab[yTemp][x] = tab[yTemp + 1][x];
              tab[yTemp + 1][x] = 0;
              yTemp--;
            }
          }
        }
      }
      return hasMoved;
    }

    function moveDown() {
      let hasMoved = false;
      for (x = 0; x < tab.length; x++) {
        for (y = tab.length - 2; y >= 0; y--) {
          if (tab[y][x] != 0) {
            let yTemp = y + 1;
            while (yTemp < tab.length && tab[yTemp][x] == 0) {
              hasMoved = true;
              tab[yTemp][x] = tab[yTemp - 1][x];
              tab[yTemp - 1][x] = 0;
              yTemp++;
            }
          }
        }
      }
      return hasMoved;
    }

    function mergeLeft() {
      let hasMerged = false;
      for (y = 0; y < 4; y++) {
        for (x = 0; x < tab.length - 1; x++) {
          if (tab[y][x] != 0 && tab[y][x] == tab[y][x + 1]) {
            hasMerged = true;
            tab[y][x] += tab[y][x + 1];
            tab[y][x + 1] = 0;
          }
        }
      }
      return hasMerged;
    }

    function mergeRight() {
      let hasMerged = false;
      for (y = 0; y < 4; y++) {
        for (x = 3; x > 0; x--) {
          if (tab[y][x] != 0 && tab[y][x] == tab[y][x - 1]) {
            hasMerged = true;
            tab[y][x] += tab[y][x - 1];
            tab[y][x - 1] = 0;
          }
        }
      }
      return hasMerged;
    }

    function mergeUp() {
      let hasMerged = false;
      for (x = 0; x < 4; x++) {
        for (y = 0; y < 3; y++) {
          if (tab[y][x] != 0 && tab[y][x] == tab[y + 1][x]) {
            hasMerged = true;
            tab[y][x] += tab[y + 1][x];
            tab[y + 1][x] = 0;
          }
        }
      }
      return hasMerged;
    }

    function mergeDown() {
      let hasMerged = false;
      for (x = 0; x < 4; x++) {
        for (y = 3; y > 0; y--) {
          if (tab[y][x] != 0 && tab[y][x] == tab[y - 1][x]) {
            hasMerged = true;
            tab[y][x] += tab[y - 1][x];
            tab[y - 1][x] = 0;
          }
        }
      }
      return hasMerged;
    }

    $(window).keydown(function(key) {
      switch (key.keyCode) {
        case 37:
          if (handleLeft()) {
            setRandomCell();
            refreshTab();
          }
          break;
        case 39:
          if (handleRight()) {
            setRandomCell();
            refreshTab();
          }
          break;
        case 38:
          if (handleUp()) {
            setRandomCell();
            refreshTab();
          }
          break;
        case 40:
          if (handleDown()) {
            setRandomCell();
            refreshTab();
          }
          break;
      }
    });
  };
})(jQuery);
